<?php
/**
 * Theme domain mapping admin settings page HTML
 *
 * @author	Sam Pospischil <pospi@spadgos.com>
 * @since	2013-10-16
 */

$themes = ThemeDomainMapping::getAvailableThemes();
$options = ThemeDomainMapping::getOptions();
$public = get_option('blog_public');

// sort themes for display in list
natcasesort($themes);

?>
<div id="theme-domain-mapping">

<h1>Theme domain mapping</h1>

<p>
The below options control which themes will be shown on each domain, and whether or not those domains should be publicly indexed with search engines.<br />The default setting for indexing according to your current blog settings is to
<?php if ($public) { echo '<strong class="enable">allow</strong> indexing'; } else { echo '<strong class="disable">disallow</strong> search engines'; } ?>
</p>
<?php if (is_multisite()) : ?>
<p>
Since you are running a multisite install, you must also map any configured domains to the site to avoid redirection. You can do this under <em><a href="<?php echo admin_url('network/settings.php?page=dm_domains_admin'); ?>">Settings > Domains</a></em> in the network admin panel.
</p>
<?php endif; ?>
<p>
Note that you must configure your own DNS externally to point any configured domains to the site.
</p>

<dl>
	<dt>Theme assignment:</dt>
	<dd>
		<form method="post">
		<ul>
			<li class="heading"><label>Theme</label><span>Domain <small>(leave empty for no assignment)</small></span></li>
		<?php foreach ($themes as $dir => $theme) : $theme = $theme['Name']; ?>
			<li>
				<label for="mapped_domains_<?php echo $dir; ?>"><?php echo $theme; ?>:</label>
				<input type="text" name="mapped_domains[<?php echo $dir; ?>]" id="mapped_domains_<?php echo $dir; ?>" value="<?php echo $options['mapped_domains'][$dir]; ?>" />

				<div class="index-settings">
					<label>Override indexing: <input type="checkbox" name="index_override[<?php echo $dir; ?>]"<?php if (isset($options['noindex_domains'][$dir])) echo ' checked="checked"'; ?> /></label>
					<div class="index-opts">
						<label><input type="radio" name="index_on[<?php echo $dir; ?>]" value="1"<?php if (isset($options['noindex_domains'][$dir]) && !empty($options['noindex_domains'][$dir])) echo ' checked="checked"'; ?> /> Enable</label>
						<label><input type="radio" name="index_on[<?php echo $dir; ?>]" value="0"<?php if (isset($options['noindex_domains'][$dir]) && empty($options['noindex_domains'][$dir])) echo ' checked="checked"'; ?> /> Disable</label>
					</div>
				</div>
			</li>
		<?php endforeach; ?>
			<li class="footer"><input type="submit" value="Save" class="button button-primary button-large" /></li>
		</ul>
		</form>
	</dd>
</dl>

</div>
